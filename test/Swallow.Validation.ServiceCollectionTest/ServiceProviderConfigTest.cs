namespace Swallow.Validation.ServiceCollectionTest
{
    using System.Linq;
    using System.Reflection;
    using Microsoft.Extensions.DependencyInjection;
    using NUnit.Framework;
    using ServiceCollection;
    using TestAsserters;

    [TestFixture]
    public sealed class ServiceProviderConfigTest
    {
        [Test]
        public void AddValidationContainer_WithAssembly_ConcreteAssertersAreCalledThroughValidationContainer()
        {
            // Arrange
            IntAsserter.ResetCalls();
            StringAsserter.ResetCalls();
            AnotherStringAsserter.ResetCalls();
            PassingAsserter.ResetCalls();
            var serviceCollection = new ServiceCollection();

            // Act
            serviceCollection.AddValidationContainer(Assembly.Load("TestAsserters"));
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // Assert
            var container = serviceProvider.GetRequiredService<ValidationContainer>();

            container.Check(new TestValue<object>("hello world", "value"), out _);
            Assert.That(IntAsserter.TimesCalled, Is.EqualTo(0));
            Assert.That(StringAsserter.TimesCalled, Is.EqualTo(1));
            Assert.That(AnotherStringAsserter.TimesCalled, Is.EqualTo(1));
            Assert.That(PassingAsserter.TimesCalled, Is.EqualTo(0));
        }

        [Test]
        public void AddValidationContainer_WithAssembly_ConcreteAssertersCanBeRetrieved()
        {
            // Arrange
            var serviceCollection = new ServiceCollection();

            // Act
            serviceCollection.AddValidationContainer(Assembly.Load("TestAsserters"));
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // Assert
            var asserters = new object[]
            {
                serviceProvider.GetService<PassingAsserter>(),
                serviceProvider.GetService<IntAsserter>(),
                serviceProvider.GetService<StringAsserter>(),
                serviceProvider.GetService<AnotherStringAsserter>()
            };

            Assert.That(asserters.All(a => a != null), Is.True);
        }

        [Test]
        public void AddValidationContainer_WithAssembly_GenericTypeIsNotRegistered()
        {
            // Arrange
            var serviceCollection = new ServiceCollection();

            // Act
            serviceCollection.AddValidationContainer(Assembly.Load("TestAsserters"));

            // Assert
            Assert.That(serviceCollection.Where(sd => sd.ServiceType.Name == typeof(AsserterWithGeneric<>).Name), Is.Empty);
        }

        [Test]
        public void AddValidationContainer_WithAssembly_ValidationContainerCanBeRetrieved()
        {
            // Arrange
            var serviceCollection = new ServiceCollection();

            // Act
            serviceCollection.AddValidationContainer(Assembly.Load("TestAsserters"));
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // Assert
            var container = serviceProvider.GetService<ValidationContainer>();
            Assert.That(container, Is.Not.Null);
        }

        private sealed class TestValue<T> : INamedValueProvider<T>
        {
            public TestValue(T value, string name)
            {
                Value = value;
                Name = name;
            }

            public T Value { get; }
            public string Name { get; }
        }
    }
}

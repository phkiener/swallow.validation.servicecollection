namespace TestAsserters
{
    using Swallow.Validation;
    using Swallow.Validation.Errors;

    /// <summary>
    ///     This is an asserter that will succeed for every object - but with a generic.
    /// </summary>
    public sealed class AsserterWithGeneric<T> : IAsserter<T>
    {
        public bool Check(INamedValueProvider<T> value, out ValidationError error)
        {
            error = null;
            return true;
        }
    }
}

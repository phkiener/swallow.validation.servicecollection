namespace TestAsserters
{
    using Swallow.Validation;
    using Swallow.Validation.Errors;

    /// <summary>
    ///     This is an asserter that will succeed for every int.
    /// </summary>
    public sealed class IntAsserter : IAsserter<int>
    {
        public static int TimesCalled { get; private set; }

        public bool Check(INamedValueProvider<int> value, out ValidationError error)
        {
            TimesCalled += 1;

            error = null;
            return true;
        }

        public static void ResetCalls()
        {
            TimesCalled = 0;
        }
    }
}

# Swallow.Validation.ServiceCollection

This is an extension to [Swallow.Validation](https://gitlab.com/phkiener/swallow.validation) allowing you to discover and register asserters in
multiple assemblies automatically. This registration will automagically register a validation container with all found asserters as well.

This extension uses an [IServiceCollection](https://docs.microsoft.com/dotnet/api/microsoft.extensions.dependencyinjection.iservicecollection?view=dotnet-plat-ext-3.1)
as container for registrations.

## Licensing

`Swallow.Validation.ServiceCollection` is licensed under the MIT license. That means you can do whatever you like with
it, as long as you give credit by including the library's license when distributing your software.

The logo (the swallow) is taken from [www.freevector.com](https://www.freevector.com/swallow-birds-vector-27800).
